import { Component } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'; 

import { ProductoBdService } from '../producto-bd.service'

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  standalone: true,
  imports: [IonicModule, ExploreContainerComponent, FormsModule, CommonModule],
  providers: [ProductoBdService]
})


export class Tab1Page {
  foto = ""
  nombre = ""
  descripcion = ""
  precio = 0.0
  disableEnviarBtn=true

  constructor(private servicio:ProductoBdService) {}
 
  

  customCounterFormatter(inputLenght:number,maxLenght:number){
    return `${maxLenght - inputLenght} caracteres disponibles`

  }


  cancelar() {
    this.foto=""
    this.nombre=""
    this.descripcion=""
    this.precio=0.0
  }

  validarEnvio(){
    console.log("Prueba")
    if (this.nombre == "" || this.descripcion == "" || this.precio==0.0){ 
      this.disableEnviarBtn=true 
     } else if (this.nombre != "" && this.disableEnviarBtn==false){

      }
      return this.disableEnviarBtn
  }

  
//
  
  cambiarFoto() {

}


  enviar(){ 
    let Datos = {
      "nombre" : this.nombre,
      "descripcion_prod" : this.descripcion,
      "precio" : this.precio
    }

  this.servicio.agregar_producto(Datos).subscribe((respuesta)=>{
    console.log(respuesta)
  }, (error)=>{
    console.log(error)
  });    
    
  }







} // termina la clase






//or= si algunos de los input esta vacio, va a desabilitar el boton
//and= si el usuario deja vacio los tres input al mismo tiempo 