import { Component, EnvironmentInjector, inject } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProductoBdService } from './producto-bd.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, HttpClientModule],

})

export class AppComponent {
  
  public environmentInjector = inject(EnvironmentInjector);
  
  url:string = "https://alaan.pythonanywhere.com/"
  
  constructor(private http:HttpClient) {}

  agregar_producto(datos:any){
    let headers = new HttpHeaders()
    headers.append("content-type", "application/json");
    return this.http.post(this.url + "post_producto", datos, {headers:headers})
  }





}

