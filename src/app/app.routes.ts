import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.routes').then((m) => m.routes),
  },

  //{
   // path: 'actualizacion',
    //loadComponent: () => import('./actualizacion/actualizacion.page').then( m => m.ActualizacionPage)
  //},
  {
    path: 'actualizar',
    loadComponent: () => import('./actualizar/actualizar.page').then( m => m.ActualizarPage)
  },
];
