import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductoBdService {

  url: string = "https://Alaan.pythonanywhere.com/"

  constructor(private http:HttpClient) { }

  agregar_producto(datos:any){
    let headers = new HttpHeaders()
    headers.append("Content-Type", "application/json");
    return this.http.post(this.url + "post_producto", datos, {headers:headers})
  }

  traer_productos(){
    return this.http.get<[]>(this.url + "traer_productos")
  }

  eliminar_producto(id:string){
    return this.http.delete(this.url + "eliminar_producto/" + id)
  }

  actualizar_producto(codigo:number, dato: any){
    return this.http.put(this.url + "actualizar_producto/" + codigo, dato)
  }







}
