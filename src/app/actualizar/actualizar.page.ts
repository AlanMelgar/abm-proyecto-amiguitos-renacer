import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ActivatedRoute, Router } from "@angular/router";
import { ProductoBdService } from "../producto-bd.service";


@Component({
  selector: 'app-actualizar',
  templateUrl: './actualizar.page.html',
  styleUrls: ['./actualizar.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule],
  providers:[ProductoBdService]
})


export class ActualizarPage implements OnInit {

  codigo:number = 0;
  nombre:string =""
  descripcion:string=""
  precio:number=0

  constructor(private activate:ActivatedRoute, private servicio:ProductoBdService, private router:Router) { }

  ngOnInit() {
    this.activate.queryParams.subscribe(parametros=>{
      console.log(parametros)
      this.codigo = parametros["codigo"]
      this.nombre = parametros["nombre"]
      this.descripcion = parametros["descripcion"]
      this.precio = parametros["precio"]
  })
  }

  actualizar(){
    let datos={
        "nombre": this.nombre,
        "descripcion": this.descripcion,
        "precio": this.precio
    }
    this.servicio.actualizar_producto(this.codigo, datos).subscribe(respuesta=>{
        console.log(respuesta)
        console.log(datos)
    })
}

cancelar(){
  this.router.navigate(["/"])
}





}
