import { TestBed } from '@angular/core/testing';

import { ProductoBdService } from './producto-bd.service';

describe('ProductoBdService', () => {
  let service: ProductoBdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductoBdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
